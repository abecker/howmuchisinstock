package org.emotioneer.howmuchisinstock.backend;

import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StockItemRepository extends MongoRepository<StockItem, String> {

  List<StockItem> findAllByStockSourceOrderByDescription();

  List<StockItem> findAllByStockLocationOrderByDescription();

}
