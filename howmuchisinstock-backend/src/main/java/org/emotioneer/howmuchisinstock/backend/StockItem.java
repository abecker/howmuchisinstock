package org.emotioneer.howmuchisinstock.backend;

import static java.lang.Integer.max;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "stockItems")
public class StockItem {

  @Id
  private String id;
  private String description;
  private int minimumStock;
  private int currentStock;
  @DBRef
  private StockSource stockSource;
  @DBRef
  private StockLocation stockLocation;

  @Transient
  public int getMissingStock() {
    return max(minimumStock - currentStock, 0);
  }

}
