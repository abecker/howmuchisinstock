package org.emotioneer.howmuchisinstock.backend;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface StockLocationRepository extends MongoRepository<StockLocation, String> {

}
