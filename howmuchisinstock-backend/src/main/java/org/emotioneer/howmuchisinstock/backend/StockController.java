package org.emotioneer.howmuchisinstock.backend;

import java.util.Random;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class StockController {

  private StockItemRepository stockItemRepository;
  private StockSourceRepository stockSourceRepository;
  private StockLocationRepository stockLocationRepository;

  public StockController(final StockItemRepository stockItemRepository,
      final StockSourceRepository stockSourceRepository,
      final StockLocationRepository stockLocationRepository) {
    this.stockItemRepository = stockItemRepository;
    this.stockSourceRepository = stockSourceRepository;
    this.stockLocationRepository = stockLocationRepository;
  }

  @GetMapping("/stockitems")
  public Iterable<StockItem> stockItems() {
    // createExampleData();
    return stockItemRepository.findAll();
  }

  private void createExampleData() {
    final var random = new Random();

    var stockSource = StockSource
        .builder()
        .description("stockSource" + random.nextInt(99999999))
        .build();
    stockSource = stockSourceRepository.save(stockSource);
    var stockLocation = StockLocation
        .builder()
        .description("stockLocation" + random.nextInt(99999999))
        .build();
    stockLocation = stockLocationRepository.save(stockLocation);
    final var stockItemRandomIdSuffix = random.nextInt(99999999);
    final var stockItem01 = StockItem
        .builder()
        .description("stockItem01" + stockItemRandomIdSuffix)
        .currentStock(2)
        .minimumStock(8)
        .stockSource(stockSource)
        .stockLocation(stockLocation)
        .build();
    stockItemRepository.save(stockItem01);
    final var stockItem02 = StockItem
        .builder()
        .description("stockItem02" + stockItemRandomIdSuffix)
        .currentStock(3)
        .minimumStock(3)
        .stockSource(stockSource)
        .stockLocation(stockLocation)
        .build();
    stockItemRepository.save(stockItem02);
  }


}
