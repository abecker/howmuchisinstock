package org.emotioneer.howmuchisinstock.backend;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface StockSourceRepository extends MongoRepository<StockSource, String> {

}
